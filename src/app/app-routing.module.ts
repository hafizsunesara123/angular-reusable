import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { ChartsComponent } from './charts/charts.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'charts',
		pathMatch: 'full'
	},
	{
		path: '',
		children: [
			{ path: 'charts', component: ChartsComponent },
		]
	},

	// // Handle all other routes
	{ path: '**', redirectTo: '' }
];

const config: ExtraOptions = {
	useHash: true,
};

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
