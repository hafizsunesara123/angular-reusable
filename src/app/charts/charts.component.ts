import { Component, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Chart } from 'angular-highcharts';
import { Options } from 'highcharts';

import * as _ from 'lodash';

import { CreateSeriesModalComponent } from '../modals/create-series-modal/create-series-modal.component';

@Component({
	selector: 'app-charts',
	templateUrl: './charts.component.html',
	styleUrls: ['./charts.component.sass']
})

export class ChartsComponent implements OnInit {
	public modalRef: BsModalRef;
	chart: Chart;
	options: Options;
	chartType = 'line';
	chartTypes = [
		{
			id: 'line',
			name: "Linechart"
		},
		{
			id: 'column',
			name: "Columnchart"
		}
	]
	modalConfigs = {
		animated: true,
		keyboard: true,
		backdrop: true,
		ignoreBackdropClick: false,
		class: "inmodal modal-dialog-centered"
	};

	constructor(
		private modalService: BsModalService
	) {
	}

	ngOnInit() {
		this.loadChart();
	}

	// --
	// Load default chart
	loadChart() {
		this.options = {
			chart: {
				type: 'line'
			},
			title: {
				text: 'Linechart'
			},
			credits: {
				enabled: false
			},
			series: [{
				name: 'Series 1',
				data: [1, 2, 3]
			}]
		};

		let chart = new Chart(this.options);
		chart.addPoint(4);
		this.chart = chart;

		chart.addPoint(5);
		setTimeout(() => {
			chart.addPoint(6);
		}, 2000);
	}

	// --
	// Add series
	addSeries() {
		this.modalRef = this.modalService.show(CreateSeriesModalComponent, this.modalConfigs);
		this.modalRef.content.event.subscribe(res => {
			this.chart.addSerie({
				name: 'Series ' + Math.floor(Math.random() * 10),
				data: res.data
			});
		});
	}

	// --
	// Remove series
	removeSeries() {
		this.chart.removeSerie(this.chart.ref.series.length - 1);
	}

	// --
	// Change chart type and update chart
	changeChartType($event) {
		this.updateChart({ chart: { type: $event.id }, title: {text: $event.name} });
	}

	private updateChart(options: Options) {
		const customizer = (_objValue: Optional, srcValue: Optional, key: any, object: any) => {
			if (srcValue === undefined) delete object[key];
		};

		const mergedOptions = _.mergeWith(this.options, options, customizer);
		this.chart = new Chart(mergedOptions);
	}
}
