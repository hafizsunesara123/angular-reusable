import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap';
import { ChartModule } from 'angular-highcharts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ChartsComponent } from './charts/charts.component';
import { CreateSeriesModalComponent } from './modals/create-series-modal/create-series-modal.component';

@NgModule({
	declarations: [
		AppComponent,
		ChartsComponent,
		CreateSeriesModalComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		NgSelectModule,
		ModalModule.forRoot(),
		ChartModule
	],
	entryComponents: [CreateSeriesModalComponent],
	providers: [],
	bootstrap: [AppComponent]
})

export class AppModule { }
