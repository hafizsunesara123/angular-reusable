import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSeriesModalComponent } from './create-series-modal.component';

describe('CreateSeriesModalComponent', () => {
  let component: CreateSeriesModalComponent;
  let fixture: ComponentFixture<CreateSeriesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSeriesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSeriesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
