import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
	selector: 'app-create-series-modal',
	templateUrl: './create-series-modal.component.html',
	styleUrls: ['./create-series-modal.component.sass']
})

export class CreateSeriesModalComponent implements OnInit {
	public event: EventEmitter<any> = new EventEmitter();
	public onClose: Subject<boolean>;
	public arrayItems: any[] = [];

	constructor (
		public bsModalRef: BsModalRef
	) {}

	ngOnInit() {
		this.onClose = new Subject();
	}

	addField() {
		this.arrayItems.push({id: ''});
	}

	removeField(i: number) {
		this.arrayItems.splice(i, 1);
	}

	createSeries() {
		console.log(this.arrayItems.length)
		if(this.arrayItems && this.arrayItems.length == 0) {
			alert('Add at least one item');
			return false;
		}

		let arrayInputs = [];
		for(var iRow in this.arrayItems) {
			arrayInputs.push(this.arrayItems[iRow].id)
		}

		this.event.emit({ data: arrayInputs });
		this.onCancel();
	}

	onCancel() {
		this.onClose.next(false);
		this.bsModalRef.hide();
	}

}
